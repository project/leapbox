;(function (window,  drupalSettings) {
  const API_SERVER_URL = 'https://api.leapbox.ai'
  const API_KEY = drupalSettings.leapbox.api_key
  const PROJECT_ID = drupalSettings.leapbox.project_id
  let PLUFIN_CONFIG = {}
  let CUSTOM_COLOR = '#5D49FF'


  function init() {
    let openChatButtonInner = `
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true" style="width: 24px; height: 24px; color: white">
          <path stroke-linecap="round" stroke-linejoin="round" d="M8.625 9.75a.375.375 0 11-.75 0 .375.375 0 01.75 0zm0 0H8.25m4.125 0a.375.375 0 11-.75 0 .375.375 0 01.75 0zm0 0H12m4.125 0a.375.375 0 11-.75 0 .375.375 0 01.75 0zm0 0h-.375m-13.5 3.01c0 1.6 1.123 2.994 2.707 3.227 1.087.16 2.185.283 3.293.369V21l4.184-4.183a1.14 1.14 0 01.778-.332 48.294 48.294 0 005.83-.498c1.585-.233 2.708-1.626 2.708-3.228V6.741c0-1.602-1.123-2.995-2.707-3.228A48.394 48.394 0 0012 3c-2.392 0-4.744.175-7.043.513C3.373 3.746 2.25 5.14 2.25 6.741v6.018z"></path>
        </svg>
        <span style="position: absolute; width: 1px; height: 1px; padding: 0; margin: -1px; overflow: hidden; clip: rect(0, 0, 0, 0); border-width: 0 ">Open Chat</span>
        `

    const closeChatButtonInner = `
      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true" style="width: 24px; height: 24px; color: white">
        <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12"></path>
      </svg>
      <span style="position: absolute; width: 1px; height: 1px; padding: 0; margin: -1px; overflow: hidden; clip: rect(0, 0, 0, 0); border-width: 0 ">Close Chat</span>
      `

    const iframe = document.createElement('iframe')
    iframe.setAttribute('id', 'leapbox-chat-ui')
    iframe.src = `https://chat.leapbox.ai?api_key=${API_KEY}&project_id=${PROJECT_ID}`
    iframe.allow = 'microphone'
    iframe.style.position = 'fixed'
    iframe.style.right = '0'
    iframe.style.zIndex = '1000'
    iframe.style.border = 'none'
    iframe.style.width = '100%'
    iframe.style.bottom = window.innerWidth < 640 ? '0' : '80px'
    iframe.style.right = window.innerWidth < 640 ? '0' : '16px'
    iframe.style.width = window.innerWidth < 640 ? '100%' : '448px'
    iframe.style.height = window.innerWidth < 640 ? '100%' : '85vh'
    iframe.style.borderRadius = window.innerWidth < 640 ? '0' : '1rem'
    iframe.style.boxShadow = '0 20px 25px -5px rgb(0 0 0 / 0.1), 0 8px 10px -6px rgb(0 0 0 / 0.1)'
    iframe.style.zIndex = '9999999'
    iframe.style.display = 'none'
    document.body.append(iframe)

    const toggleButton = document.createElement('button')
    toggleButton.setAttribute('id', 'leapbox-chat-button')
    toggleButton.style.overflow = 'hidden'
    toggleButton.innerHTML = openChatButtonInner
    toggleButton.style.padding = '0'
    toggleButton.style.backgroundColor = CUSTOM_COLOR
    toggleButton.style.color = '#fff'
    toggleButton.style.borderRadius = '50%'
    toggleButton.style.position = 'fixed'
    toggleButton.style.display = 'flex'
    toggleButton.style.justifyContent = 'center'
    toggleButton.style.alignItems = 'center'
    toggleButton.style.bottom = '16px'
    toggleButton.style.right = '16px'
    toggleButton.style.width = '48px'
    toggleButton.style.height = '48px'
    toggleButton.style.zIndex = '9999998'
    toggleButton.style.border = 'none'
    toggleButton.style.cursor = 'pointer'
    toggleButton.onclick = () => {
      if (iframe.style.display === 'none') {
        iframe.contentWindow.postMessage(
          {
            type: 'openChat',
            params: PLUFIN_CONFIG
          },
          '*'
        )
        iframe.style.display = 'block'
        toggleButton.innerHTML = closeChatButtonInner
      } else {
        iframe.contentWindow.postMessage({ type: 'closeChat' }, '*')
        iframe.style.display = 'none'
        toggleButton.innerHTML = openChatButtonInner
      }
    }

    if (window.innerWidth >= 320) {
      document.body.appendChild(toggleButton)
    }

    fetch(`${API_SERVER_URL}/api/get_project_plugin/${PROJECT_ID}`)
      .then((response) => response.json())
      .then((res) => {
        if (res.success) {
          const { color, widget } = res.data
          PLUFIN_CONFIG = res.data
          if (widget) {
            openChatButtonInner = `<img src="${widget}" alt="" style="width: 100%; height: 100%;" />`
            toggleButton.innerHTML = openChatButtonInner
          } else if (color) {
            CUSTOM_COLOR = color
            toggleButton.style.backgroundColor = color
          }
        }
      })
      .catch((error) => console.log(error))

    // Update iframe height on window resize
    window.addEventListener('resize', () => {
      iframe.style.bottom = window.innerWidth < 640 ? '0' : '5rem'
      iframe.style.right = window.innerWidth < 640 ? '0' : '1rem'
      iframe.style.width = window.innerWidth < 640 ? '100%' : '448px'
      iframe.style.height = window.innerWidth < 640 ? '100%' : '85vh'
      iframe.style.borderRadius = window.innerWidth < 640 ? '0' : '0.75rem'
    })

    window.addEventListener('message', (event) => {
      if (event.origin !== 'https://chat.leapbox.ai') {
        return
      }
      if (event.data.isOpen) {
        iframe.style.display = 'block'
        toggleButton.innerHTML = closeChatButtonInner
      } else {
        iframe.style.display = 'none'
        toggleButton.innerHTML = openChatButtonInner
      }
    })

    window.$leapbox = {
      updateInfo: function (params) {
        iframe.contentWindow.postMessage(
          {
            type: 'updateInfo',
            params
          },
          '*'
        )
      }
    }
  }

  if (document.readyState === 'complete') {
    init()
  } else {
    window.addEventListener('load', init)
  }
})(window, drupalSettings)
