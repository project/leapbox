<?php

namespace Drupal\LeapBox\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\leapbox\LeapBoxApiClient;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LeapBoxSettingsForm extends ConfigFormBase
{

  protected function getEditableConfigNames()
  {
    return [
      'leapbox.settings',
    ];
  }

  public function getFormId()
  {
    return 'leapbox_settings_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $config = $this->config('leapbox.settings');

    $form['config']  = [
      '#type' => 'fieldset',
      '#title' => $this->t('LeapBox Configuration'),
      '#predix' => '<div id="fieldset-wrapper">',
      '#suffix' =>  '</div>'
    ];

    $form['config']['api_key'] = [
      '#type' => 'textfield',
      '#id' =>  'api_key',
      '#title' => $this->t('Api Key'),
      '#required' => TRUE,
      '#description' =>  $this->t('Enter the LeapBox project api key'),
      '#default_value' => !empty($config) ? $config->get('api_key') : '',
    ];

    $form['config']['project_id'] = [
      '#type' => 'textfield',
      '#id' =>  'project_id',
      '#title' => $this->t('Project ID'),
      '#required' => TRUE,
      '#description' =>  $this->t('Enter the LeapBox project id'),
      '#default_value' => !empty($config) ? $config->get('project_id') : '',
    ];

    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $this->config('leapbox.settings')
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('project_id', $form_state->getValue('project_id'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
