# leapbox

LeapBox is a fully conversational and Multilingual AI Assistant powered by OpenAI's GPT-3. LeapBox chat bots can recognize and speak your native language. You can use it to automate your customer support, online applications, searching through internal knowledge base and many other tasks.

### How to use

- Clone this project
- Move this project to your Drupal module
- Start your Drupal project, and to Extend install LeapBox, set configuration
